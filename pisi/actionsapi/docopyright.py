#!/usr/bin/python2
# -*- coding: utf-8 -*-
#
#  docopyright.py
#  
#  Copyright 2023 fury <uglyside@yandex.ru>
#  
#  This file is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  

import sys
import os
from pisi.actionsapi import get
from pisi.actionsapi.shelltools import ls, isFile, isLink, chmod
from pisi.actionsapi.pisitools import insinto

matches = [
'Author', 
'Copy', 
'License', 
'Thank', 
'AUTHOR', 
'COPY', 
'LICENSE', 
'THANK', 
'CREDIT', 
'META.yml', 
]

def installCopyright():
	for t in ls('.'):
		if os.stat(t).st_size == 0:
			continue

		if t.startswith(tuple(matches)) and isFile(t):
			if isLink(t):
				srcfile = os.readlink(t)
			else:
				srcfile = t
			try:
				chmod(srcfile, mode = 0644)
				insinto('/usr/share/copyright/%s' % get.srcNAME(), srcfile)
			except AttributeError, e:
				print e
